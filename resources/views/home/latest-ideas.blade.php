<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Latest Ideas</h3>
    </div>

    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Idea</th>
                    <th>Date</th>
                </tr>
                @foreach($latestIdeas as $idea)
                    <tr>
                        <td>{{ $idea['content'] }}</td>
                        <td width="97">{{ date('M d, Y', $idea['created_at']->timestamp) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
</div>