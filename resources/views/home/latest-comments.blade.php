<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Latest Comments</h3>
    </div>

    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Comment</th>
                    <th>Date</th>
                </tr>
                @foreach($latestComments as $comment)
                    <tr>
                        <td>{{ $comment['content'] }}</td>
                        <td width="97">{{ date('M d, Y', $comment['created_at']->timestamp) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
</div>