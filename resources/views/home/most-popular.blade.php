<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Most Popular Ideas</h3>
    </div>

    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Idea</th>
                    <th>Liked</th>
                </tr>
                @foreach($mostPopular as $idea)
                    <tr>
                        <td>{{ $idea['content'] }}</td>
                        <td>{{ $idea['total_up'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
</div>