<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model {
	protected $table = 'rates';
	protected $fillable = [ 'id', 'idea_id', 'user_id', 'type' ];
	public $timestamps = true;

	public function idea() {
		return $this->belongsTo( 'App\Models\Idea' );
	}

	public function owner() {
		return $this->belongsTo( 'Encore\Admin\Auth\Database\Administrator' );
	}
}
