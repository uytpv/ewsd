<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {
	protected $table = 'comments';
	protected $fillable = [ 'id', 'content', 'status', 'user_id', 'idea_id', 'isAnonymous' ];
	public $timestamps = true;

	public function idea() {
		return $this->belongsTo( 'App\Models\Idea' );
	}

	public function owner() {
		return $this->belongsTo( 'Encore\Admin\Auth\Database\Administrator' );
	}
}
