<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	protected $table = 'categories';
	protected $fillable = [ 'id', 'category_name' ];
	public $timestamps = true;

	public function ideas() {
		return $this->hasMany( 'App\Models\Idea' );
	}

	public function owner() {
		return $this->belongsTo( 'Encore\Admin\Auth\Database\Administrator' );
	}
}
