<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model {
	protected $table = 'ideas';
	protected $fillable = [ 'id', 'content', 'status', 'total_up', 'total_down', 'isAnonymous', 'cate_id', 'user_id' ];
	public $timestamps = true;
	public function category() {
		return $this->belongsTo( 'App\Models\Category' );
	}

	public function owner() {
		return $this->belongsTo( 'Encore\Admin\Auth\Database\Administrator' );
	}

	public function documents() {
		return $this->hasMany( 'App\Models\Document' );
	}

	public function comments() {
		return $this->hasMany( 'App\Models\Comment' );
	}

	public function rates() {
		return $this->hasMany( 'App\Models\Rate' );
	}
}
