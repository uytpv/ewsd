<?php

namespace App\Admin\Services;

use Illuminate\Support\Facades\Mail;

class MailService {

	public static function send( $template, $user, $subject ) {
		Mail::send( [ 'html' => $template ], [ 'name' => $user->name ], function ( $message ) use ( $subject, $user ) {
			$message->to( $user->email, $user->name )->subject( $subject );
			$message->from( 'ewsdproject@fpt.edu.vn', 'EWSD Administrator' );
		} );
	}
}