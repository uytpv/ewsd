<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group( [
	'prefix'     => config( 'admin.route.prefix' ),
	'namespace'  => config( 'admin.route.namespace' ),
	'middleware' => config( 'admin.route.middleware' ),
], function ( Router $router ) {

	$router->get( '/', 'HomeController@index' );
	$router->get( 'analyze', 'ReportController@index' );
	$router->resource( 'category', CategoryController::class );
	$router->resource( 'idea', IdeaController::class );
	$router->resource( 'comment', CommentController::class );
	$router->resource( 'document', DocumentController::class );
	$router->resource( 'config', ConfigController::class );

	$router->get( 'api/rate/{ideaId}/{type}', 'RateApiController@rate' );
} );

