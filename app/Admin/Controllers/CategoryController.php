<?php

namespace App\Admin\Controllers;

use App\Models\Category;

use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CategoryController extends Controller {
	use ModelForm;

	/**
	 * Index interface.
	 *
	 * @return Content
	 */
	public function index() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Categories' );
			$content->description( 'List' );

			$content->body( $this->grid() );
		} );
	}

	/**
	 * Edit interface.
	 *
	 * @param $id
	 *
	 * @return Content
	 */
	public function edit( $id ) {
		return Admin::content( function ( Content $content ) use ( $id ) {

			$content->header( 'Category' );
			$content->description( 'Edit' );

			$content->body( $this->form()->edit( $id ) );
		} );
	}

	/**
	 * Create interface.
	 *
	 * @return Content
	 */
	public function create() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Category' );
			$content->description( 'Create' );

			$content->body( $this->form() );
		} );
	}

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid() {
		return Admin::grid( Category::class, function ( Grid $grid ) {

			$grid->id( 'ID' )->sortable();
			$grid->category_name( 'Name' )->sortable();
			$grid->column( 'user_id', 'Managed By' )->display( function ( $user_id ) {
				$user = Administrator::find( $user_id );

				return $user->name;
			} );

			$grid->created_at();
			$grid->updated_at();
		} );
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form() {
		return Admin::form( Category::class, function ( Form $form ) {

			$form->display( 'id', 'ID' );
			$form->text( 'category_name', 'Category Name' )
			     ->help( 'Required' )
			     ->rules( 'required|unique:' . env( 'DB_CONNECTION' ) . '.' . env( 'DB_DATABASE' ) . '.categories,category_name' );

			$role = Role::where('slug', 'coordinator')->first();
			$option = $role->administrators->pluck( 'name', 'id' );

			$form->select('user_id', 'Managed By')->options($option)->rules('required');

			$form->display( 'created_at', 'Created At' );
			$form->display( 'updated_at', 'Updated At' );
		} );
	}
}
