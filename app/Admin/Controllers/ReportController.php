<?php

namespace App\Admin\Controllers;

use App\Models\Comment;
use App\Models\Idea;
use App\Models\Rate;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\InfoBox;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller {
	public function index() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Analysis' );

			$content->row( function ( $row ) {
				$row->column( 4, new InfoBox( 'Total Ideas', 'lightbulb-o', 'yellow', '/admin/idea', Idea::count() ) );
				$row->column( 4, new InfoBox( 'Total Comments', 'comment', 'green', '', Comment::count() ) );
				$row->column( 4, new InfoBox( 'Total Rating', 'thumbs-up', 'blue', '', Rate::count() ) );
			} );
		} );
	}
}
