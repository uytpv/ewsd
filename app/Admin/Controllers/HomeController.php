<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Idea;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller {
	public function index() {
		return Admin::content( function ( Content $content ) {
			$content->row( $this->title() );
			$content->row( function ( Row $row ) {
				$row->column( 4, function ( Column $column ) {
					$column->append( $this->mostPopular() );
				} );
				$row->column( 4, function ( Column $column ) {
					$column->append( $this->latestIdeas() );
				} );
				$row->column( 4, function ( Column $column ) {
					$column->append( $this->latestComments() );
				} );
			} );
		} );
	}

	public static function title() {
		return view( 'home.title' );
	}

	public static function mostPopular() {
		$mostPopular = Idea::all()->sortByDesc( 'total_up' )->take( 10 );

		return view( 'home.most-popular', compact( 'mostPopular' ) );
	}

	public static function latestIdeas() {
		$latestIdeas = Idea::all()->sortByDesc( 'created_at' )->take( 10 );

		return view( 'home.latest-ideas', compact( 'latestIdeas' ) );
	}

	public static function latestComments() {
		$latestComments = Comment::all()->sortByDesc( 'created_at' )->take( 10 );

		return view( 'home.latest-comments', compact( 'latestComments' ) );
	}
}
