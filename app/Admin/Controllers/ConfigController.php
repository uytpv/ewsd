<?php

namespace App\Admin\Controllers;

use App\Models\Config;

use Carbon\Carbon;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Input;

class ConfigController extends Controller {
	use ModelForm;

	/**
	 * Index interface.
	 *
	 * @return Content
	 */
	public function index() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Date config' );
			$content->description( '' );

			$content->body( $this->grid() );
		} );
	}

	/**
	 * Edit interface.
	 *
	 * @param $id
	 *
	 * @return Content
	 */
	public function edit( $id ) {
		return Admin::content( function ( Content $content ) use ( $id ) {

			$content->header( 'Date Config' );
			$content->description( 'Edit' );

			$content->body( $this->form()->edit( $id ) );
		} );
	}

	/**
	 * Create interface.
	 *
	 * @return Content
	 */
	public function create() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Date Config' );
			$content->description( 'Create' );

			$content->body( $this->form() );
		} );
	}

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid() {
		return Admin::grid( Config::class, function ( Grid $grid ) {

			$grid->key( 'KEY ' );
			$grid->value( 'VALUE ' )->display( function () {
				return date( 'M d, y', $this->value );
			} );

			$grid->created_at();
			$grid->updated_at();
		} );
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form() {
		return Admin::form( Config::class, function ( Form $form ) {

			$form->display( 'id', 'ID' );
			$form->text( 'key', 'KEY' )->rules( 'required' );
			$form->date( 'value', 'VALUE' )->rules( 'required' );

			$form->display( 'created_at', 'Created At' );
			$form->display( 'updated_at', 'Updated At' );
			$form->saving( function ( Form $f ) {
				$value = Input::get( 'value' );
				$f->input( 'value', Carbon::parse( $value, config( 'app.timezone' ) )->getTimestamp() );
			} );
		} );
	}

	public function setEnvironmentValue( $envKey, $envValue ) {
		$envFile = app()->environmentFilePath();
		$str     = file_get_contents( $envFile );

		$oldValue = strtok( $str, $envKey . '=' );

		$str = str_replace( $envKey . '=' . $oldValue, $envKey . '=' . $envValue . '\n', $str );

		$fp = fopen( $envFile, 'w' );
		fwrite( $fp, $str );
		fclose( $fp );
	}
}
