<?php

namespace App\Admin\Controllers;

use App\Admin\Services\MailService;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Config;
use App\Models\Document;
use App\Models\Idea;

use Carbon\Carbon;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Mail;

class IdeaController extends Controller {
	use ModelForm;

	/**
	 * Index interface.
	 *
	 * @return Content
	 */
	public function index() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Idea List' );
			$content->description( 'The closure date: ' . date( 'M d, Y H:i:s', Config::where( 'key', '=', 'CLOSURE_DATE' )->first()->value ) );

			$content->body( $this->grid() );
		} );
	}

	/**
	 * Edit interface.
	 *
	 * @param $id
	 *
	 * @return Content
	 */
	public function edit( $id ) {
		return Admin::content( function ( Content $content ) use ( $id ) {

			$content->header( 'Idea' );
			$content->description( 'Edit an idea' );

			$content->body( $this->form()->edit( $id ) );
		} );
	}

	/**
	 * Create interface.
	 *
	 * @return Content
	 */
	public function create() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Idea' );
			$content->description( 'Create your idea' );

			$content->body( $this->form() );
		} );
	}

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid() {
		return Admin::grid( Idea::class, function ( Grid $grid ) {
//			$grid->id( 'ID' )->sortable();

			$grid->model()->orderBy( 'created_at', 'DESC' );

			$grid->column( 'Idea' )->display( function () {
				$postBy = $this->isAnonymous ? 'Anonymous' : Administrator::find( $this->user_id )->name;
				if ( Admin::user()->isRole( 'student' ) ) {
					$tt           = Comment::where( 'idea_id', '=', $this->id )->count();
					$totalComment = $tt - Comment::join( 'admin_role_users', 'comments.user_id', '=', 'admin_role_users.user_id' )
					                             ->where( 'comments.idea_id', '=', $this->id )
					                             ->where( 'admin_role_users.role_id', '<>', 2 )
					                             ->count();
				} else {
					$totalComment = Comment::where( 'idea_id', '=', $this->id )->count();
				}

				$totalDocument = Document::where( 'idea_id', '=', $this->id )->count();
				$content       = '
		            <p>' . $this->content . '</p>
		            <p>----------------------------------------</p>
		            <p><a href="/admin/comment?ideaId=' . $this->id . '">View ' . $totalComment . ' comment(s)</a> | Like: ' . $this->total_up . ' | Dislike: ' . $this->total_down . ' </p>
		            <p>by <a href="">' . $postBy . '</a> | Posted on ' . date( 'M d, Y H:i:s' ) . '</p>
		            <p><a href="/admin/document?ideaId=' . $this->id . '">View ' . $totalDocument . ' document(s)</a></p>';

				return $content;
			} )->setAttributes( [ 'width' => ' 70%' ] );


			$grid->tools( function ( $tools ) {
				$tools->append( '<a href="/admin/idea/create" class="btn btn-sm btn-success"><i class="fa fa-lightbulb-o"></i> Post your idea</a>' );
			} );
			$grid->disableFilter();
			$grid->disableRowSelector();
			$grid->disableExport();
			$grid->disableCreateButton();
			$grid->actions( function ( $actions ) {
				$actions->disableDelete();
				$actions->disableEdit();
				$actions->prepend( '<p><a href="/admin/api/rate/' . $actions->getKey() . '/0" class="btn btn-xs btn-primary"><i class="fa fa-thumbs-down"></i> Dislike</a></p>' );
				$actions->prepend( '<p><a href="/admin/api/rate/' . $actions->getKey() . '/1" class="btn btn-xs btn-primary"><i class="fa fa-thumbs-up"></i> Like</a></p>' );
				$actions->prepend( '<p><a href="/admin/document/create?ideaId=' . $actions->getKey() . '" class="btn btn-xs btn-primary"><i class="fa fa-file-archive-o"></i> Document</a></p>' );
				$actions->prepend( '<p><a href="/admin/comment/create?ideaId=' . $actions->getKey() . '" class="btn btn-xs btn-success"><i class="fa fa-comment"></i> Comment</a></p>' );
			} );

			$grid->paginate( 5 );
		} );
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form() {
		return Admin::form( Idea::class, function ( Form $form ) {
			$closureDate = Config::where( 'key', '=', 'CLOSURE_DATE' )->first();
			if ( Carbon::now()->getTimestamp() > (int) $closureDate->value ) {
				$form->tools( function ( Form\Tools $tools ) {
					$tools->disableListButton();
				} );
				$form->disableSubmit();
				$form->disableReset();

				$form->html( '<h4>Can not post or edit an idea any more</h4>' );
			} else {
				$form->hidden( 'id', 'ID' );
				$categories = Category::all()->pluck( 'category_name', 'id' );
				$form->select( 'cate_id', 'Category' )->options( $categories )->rules( 'required' );
				$form->textarea( 'content' )->rules( 'required|min:50' )->help( 'Content length must greater than 50 characters' );
				$form->switch( 'isAnonymous', 'Anonymous posting' );
				$form->checkbox( 'agree', 'Agreement' )->options( [ 1 => 'I have read and agree to the' ] )->rules( 'required' );
				$form->html( '<a href="http://google.com" target="_blank" style="top: -15px; left: 55px; position: absolute">Terms and Conditions agreement</a>' );
				$form->ignore( [ 'agree' ] );
				$form->hidden( 'user_id' )->default( Admin::user()->id );
				$form->hidden( 'status' )->default( - 1 );
				$form->hidden( 'total_up' )->default( 0 );
				$form->hidden( 'total_down' )->default( 0 );
				$form->hidden( 'created_at', 'Created At' );
				$form->hidden( 'updated_at', 'Updated At' );

				$form->saved( function ( Form $f ) {
					$cateIdBelong = $f->model()->cate_id;
					$userId       = Category::find( $cateIdBelong )->user_id;
					$coordinator  = Administrator::find( $userId );

					MailService::send( 'mails.notify-new-idea', $coordinator, 'New idea has just submited' );
				} );
			}
		} );
	}
}
