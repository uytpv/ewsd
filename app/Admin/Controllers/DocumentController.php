<?php

namespace App\Admin\Controllers;

use App\Models\Comment;
use App\Models\Document;

use App\Models\Idea;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Widgets\Collapse;

class DocumentController extends Controller {
	use ModelForm;

	/**
	 * Index interface.
	 *
	 * @return Content
	 */
	public function index() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Document' );
			$content->description( 'list documents' );

			$content->body( $this->grid() );
		} );
	}

	/**
	 * Edit interface.
	 *
	 * @param $id
	 *
	 * @return Content
	 */
	public function edit( $id ) {
		return Admin::content( function ( Content $content ) use ( $id ) {

			$content->header( 'Document' );
			$content->description( 'edit a document' );

			$content->body( $this->form()->edit( $id ) );
		} );
	}

	/**
	 * Create interface.
	 *
	 * @return Content
	 */
	public function create() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Document' );
			$content->description( 'upload new document' );

			$content->body( $this->form() );
		} );
	}

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid() {
		return Admin::grid( Document::class, function ( Grid $grid ) {

			$ideaId = key_exists( 'ideaId', $_REQUEST ) ? $_REQUEST['ideaId'] : null;
			if ( $ideaId == null ) {
				return redirect( '/admin/idea' );
			}
			$idea   = Idea::find( $ideaId );
			$postBy = $idea->isAnonymous ? 'Anonymous' : Administrator::find( $idea->user_id )->name;
			if ( Admin::user()->isRole( 'student' ) ) {
				$totalComment = Comment::join( 'admin_role_users', 'comments.user_id', '=', 'admin_role_users.user_id' )
				                       ->where( 'comments.idea_id', '=', $idea->id )
				                       ->where( 'admin_role_users.role_id', '<>', 2 )
				                       ->count();
			} else {
				$totalComment = Comment::where( 'idea_id', '=', $idea->id )->count();
			}

			$totalDocument = Document::where( 'idea_id', '=', $idea->id )->count();
			$content       = '
		            <p>' . $idea->content . '</p>
		            <p>----------------------------------------</p>
		            <p><a href="/admin/comment?ideaId=' . $idea->id . '">View ' . $totalComment . ' comment(s)</a> | Like: ' . $idea->total_up . ' | Dislike: ' . $idea->total_down . ' </p>
		            <p>by <a href="">' . $postBy . '</a> | Posted on ' . date( 'M d, Y H:i:s' ) . '</p>
		            <p><a href="/admin/document?ideaId=' . $idea->id . '">View ' . $totalDocument . ' document(s)</a></p>';

			$collapse = new Collapse();
			$collapse->add( 'Idea', $content );
			echo $collapse->render();
			$grid->model()->where( 'idea_id', '=', $ideaId );
			$grid->column( 'File' )->display( function () {
				return '<a href="' . env( 'APP_URL' ) . ':' . env( 'APP_PORT' ) . '/uploads/' . $this->file . '" download target="_blank">' . $this->document_name . '</a>';
			} );
			$grid->created_at();
			$grid->tools( function ( $tools ) use ( $ideaId ) {
				$tools->append( '<a href="/admin/document/create?ideaId=' . $ideaId . '" class="btn btn-sm btn-success"><i class="fa fa-comment"></i> Upload Document</a>' );
			} );
			$grid->disableActions();
			$grid->disableRowSelector();

			$grid->disableCreateButton();
			$grid->disableExport();
			$grid->disableFilter();
		} );
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form() {
		return Admin::form( Document::class, function ( Form $form ) {

			$ideaId = key_exists( 'ideaId', $_REQUEST ) ? $_REQUEST['ideaId'] : null;
			$form->text( 'document_name', 'Document Name' )->rules( 'required|max:60|min:3' );
			$form->file( 'file', 'File' )->rules( 'required|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,txt' )->help( 'Support file types: jpg, jpeg, bmp, png, pdf, doc, docx, xls, xlsx, txt' )->uniqueName();
			$form->hidden( 'idea_id' )->default( $ideaId );
			$form->display( 'created_at', 'Created At' );
			$form->display( 'updated_at', 'Updated At' );
			$form->saved( function ( Form $f ) {
				$ideaId = $f->input( 'idea_id' );
				admin_toastr( 'Upload document success' );

				return redirect( '/admin/document?ideaId=' . $ideaId );
			} );
		} );
	}
}
