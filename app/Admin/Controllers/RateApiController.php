<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Idea;
use App\Models\Rate;
use Carbon\Carbon;
use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;

class RateApiController extends Controller {
	public function rate( Request $request ) {
		$userId = Admin::user()->id;
		$ideaId = $request->route( 'ideaId' );
		$idea   = Idea::find( $ideaId );
		$type   = $request->route( 'type' );

		$rate = Rate::where( 'idea_id', '=', $ideaId )->where( 'user_id', '=', $userId )->first();
		if ( $rate === null ) {
			$rate             = new Rate();
			$rate->idea_id    = $ideaId;
			$rate->user_id    = $userId;
			$rate->type       = $type;
			$rate->created_at = Carbon::now()->getTimestamp();
			if ( $type === '1' ) {
				$idea->total_up = $idea->total_up + 1;
			} else {
				$idea->total_down = $idea->total_down + 1;
			}
			admin_toastr( $type == '1' ? 'Like success!' : 'Dislike success!' );
		} else {
			if ( $rate->type <> $type ) {
				$rate->type = $type;
				if ( $type === '1' ) {
					$idea->total_up   = $idea->total_up + 1;
					$idea->total_down = $idea->total_down - 1;
				} else {
					$idea->total_down = $idea->total_down + 1;
					$idea->total_up   = $idea->total_up - 1;
				}
				admin_toastr( $type == '1' ? 'Like success!' : 'Dislike success!' );
			} else {
				admin_toastr( 'Already rate this idea before!' );
			}
		}
		$idea->save();
		$rate->save();

		return back();
	}
}