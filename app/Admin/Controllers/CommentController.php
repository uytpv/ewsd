<?php

namespace App\Admin\Controllers;

use App\Admin\Services\MailService;
use App\Models\Comment;

use App\Models\Config;
use App\Models\Document;
use App\Models\Idea;
use Carbon\Carbon;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Widgets\Collapse;

class CommentController extends Controller {
	use ModelForm;

	/**
	 * Index interface.
	 *
	 * @return Content
	 */
	public function index() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Comment' );
			$content->description( 'list' );

			$content->body( $this->grid() );
		} );
	}

	/**
	 * Edit interface.
	 *
	 * @param $id
	 *
	 * @return Content
	 */
	public function edit( $id ) {
		return Admin::content( function ( Content $content ) use ( $id ) {

			$content->header( 'Comment' );
			$content->description( 'Edit your comment' );

			$content->body( $this->form()->edit( $id ) );
		} );
	}

	/**
	 * Create interface.
	 *
	 * @return Content
	 */
	public function create() {
		return Admin::content( function ( Content $content ) {

			$content->header( 'Comment' );
			$content->description( 'Send your new comment' );

			$content->body( $this->form() );
		} );
	}

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid() {
		return Admin::grid( Comment::class, function ( Grid $grid ) {
			$ideaId = key_exists( 'ideaId', $_REQUEST ) ? $_REQUEST['ideaId'] : null;
			if ( $ideaId == null ) {
				return redirect( '/admin/idea' );
			}

			$idea   = Idea::find( $ideaId );
			$postBy = $idea->isAnonymous ? 'Anonymous' : Administrator::find( $idea->user_id )->name;
			if ( Admin::user()->isRole( 'student' ) ) {
				$totalComment = Comment::join( 'admin_role_users', 'comments.user_id', '=', 'admin_role_users.user_id' )
				                       ->where( 'comments.idea_id', '=', $idea->id )
				                       ->where( 'admin_role_users.role_id', '<>', 2 )
				                       ->count();
			} else {
				$totalComment = Comment::where( 'idea_id', '=', $idea->id )->count();
			}

			$totalDocument = Document::where( 'idea_id', '=', $idea->id )->count();
			$content       = '
		            <p>' . $idea->content . '</p>
		            <p>----------------------------------------</p>
		            <p><a href="/admin/comment?ideaId=' . $idea->id . '">View ' . $totalComment . ' comment(s)</a> | Like: ' . $idea->total_up . ' | Dislike: ' . $idea->total_down . ' </p>
		            <p>by <a href="">' . $postBy . '</a> | Posted on ' . date( 'M d, Y H:i:s' ) . '</p>
		            <p><a href="/admin/document?ideaId=' . $idea->id . '">View ' . $totalDocument . ' document(s)</a></p>';

			$collapse = new Collapse();
			$collapse->add( 'Idea', $content );
			echo $collapse->render();

			if ( Admin::user()->inRoles( [ 'coordinator', 'manager', 'administrator' ] ) ) {
				$grid->model()->where( 'idea_id', '=', $ideaId );
			}

			if ( Admin::user()->isRole( 'student' ) ) {
				$grid->model()
				     ->join( 'admin_role_users', 'comments.user_id', '=', 'admin_role_users.user_id' )
				     ->join( 'admin_roles', 'admin_role_users.role_id', '=', 'admin_roles.id' )
				     ->where( 'admin_roles.slug', '=', 'student' )
				     ->where( 'idea_id', '=', $ideaId );
			}

			$grid->content();
			$grid->column( 'From' )->display( function () {
				return $this->isAnonymous ? 'Anonymous' : Administrator::find( $this->user_id )->name;
			} );
			$grid->created_at( 'Created At' );
			$grid->tools( function ( $tools ) use ( $ideaId ) {
				$tools->append( '<a href="/admin/comment/create?ideaId=' . $ideaId . '" class="btn btn-sm btn-success"><i class="fa fa-comment"></i> Comment</a>' );
//				$tools->append( '<a href="javascript:void(0);" class="btn btn-sm btn-primary"><i class="fa fa-thumbs-up"></i></a>' );
//				$tools->append( '<a href="javascript:void(0);" class="btn btn-sm btn-primary"><i class="fa fa-thumbs-down"></i></a>' );
			} );

			$grid->disableActions();
			$grid->disableRowSelector();

			$grid->disableCreateButton();
			$grid->disableExport();
			$grid->disableFilter();

		} );
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form() {
		return Admin::form( Comment::class, function ( Form $form ) {
			$finalClosureDate = Config::where( 'key', '=', 'FINAL_CLOSURE_DATE' )->first();
			if ( Carbon::now()->getTimestamp() > (int) $finalClosureDate->value ) {
				$form->tools( function ( Form\Tools $tools ) {
					$tools->disableListButton();
				} );
				$form->disableSubmit();
				$form->disableReset();

				$form->html( '<h4>Can not send comment any more</h4>' );
			} else {
				$ideaId = key_exists( 'ideaId', $_REQUEST ) ? $_REQUEST['ideaId'] : null;
				$form->textarea( 'content', 'Comment' )->rules( 'required|min:20' )->help( 'Comment length must greater than 20 characters' );
				$form->switch( 'isAnonymous', 'Anonymous posting' );
				$form->hidden( 'idea_id' )->default( $ideaId );
				$form->hidden( 'status' )->default( - 1 );
				$form->hidden( 'user_id' )->default( Admin::user()->id );
				$form->display( 'created_at', 'Created At' );
				$form->display( 'updated_at', 'Updated At' );
				$form->saved( function ( Form $f ) {
					$userId       = $f->model()->user_id;
					$ideaId       = $f->model()->idea_id;
					$idea         = Idea::find( $ideaId );
					$ideaOwner    = Administrator::find( $idea->user_id );
					$commentOwner = Administrator::find( $userId );

					if ( $commentOwner->isRole( 'student' ) ) {
						MailService::send( 'mails.notify-new-comment', $ideaOwner, 'Your idea got new comment!' );
					}

					$ideaId = $f->input( 'idea_id' );
					admin_toastr( 'Success' );

					return redirect( '/admin/comment?ideaId=' . $ideaId );
				} );
			}
		} );
	}
}
