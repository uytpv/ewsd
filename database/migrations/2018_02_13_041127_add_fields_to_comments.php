<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToComments extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'comments', function ( Blueprint $table ) {
			$table->string( 'content' );
			$table->smallInteger( 'status' );
			$table->integer( 'user_id' )->unsigned();
			$table->foreign( 'user_id' )->references( 'id' )->on( 'admin_users' )->onDelete( 'cascade' );
			$table->integer( 'idea_id' )->unsigned();
			$table->foreign( 'idea_id' )->references( 'id' )->on( 'ideas' )->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'comments', function ( Blueprint $table ) {
			$table->dropColumn( 'content' );
			$table->dropColumn( 'status' );
			$table->dropForeign( 'user_id' )->references( 'id' )->on( 'admin_users' )->onDelete( 'cascade' );
			$table->dropForeign( 'idea_id' )->references( 'id' )->on( 'ideas' )->onDelete( 'cascade' );
			$table->dropColumn( 'user_id' )->unsigned();
			$table->dropColumn( 'idea_id' )->unsigned();
		} );
	}
}
