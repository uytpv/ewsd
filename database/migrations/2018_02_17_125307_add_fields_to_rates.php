<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToRates extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'rates', function ( Blueprint $table ) {
			$table->integer( 'idea_id' )->unsigned();
			$table->foreign( 'idea_id' )->references( 'id' )->on( 'ideas' )->onDelete( 'cascade' );
			$table->integer( 'user_id' )->unsigned();
			$table->foreign( 'user_id' )->references( 'id' )->on( 'admin_users' )->onDelete( 'cascade' );
			$table->string( 'type' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'rates', function ( Blueprint $table ) {
			$table->dropColumn( 'type' );
			$table->dropForeign( 'user_id' )->references( 'id' )->on( 'admin_users' )->onDelete( 'cascade' );
			$table->dropForeign( 'idea_id' )->references( 'id' )->on( 'ideas' )->onDelete( 'cascade' );
			$table->dropColumn( 'user_id' )->unsigned();
			$table->dropColumn( 'idea_id' )->unsigned();
		} );
	}
}
