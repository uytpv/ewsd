<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDocuments extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'documents', function ( Blueprint $table ) {
			$table->string( 'file' );
			$table->integer( 'idea_id' )->unsigned();
			$table->foreign( 'idea_id' )->references( 'id' )->on( 'ideas' )->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'documents', function ( Blueprint $table ) {
			$table->dropColumn( 'file' );
			$table->dropForeign( 'idea_id' )->references( 'id' )->on( 'ideas' )->onDelete( 'cascade' );
			$table->dropColumn( 'idea_id' )->unsigned();
		} );
	}
}