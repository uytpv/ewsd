<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdeasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ideas', function (Blueprint $table) {
	        $table->increments( 'id' );
	        $table->text( 'content' );
	        $table->smallInteger( 'status' );
	        $table->integer( 'total_up' );
	        $table->integer( 'total_down' );
	        $table->boolean( 'isAnonymous' );
	        $table->integer( 'cate_id' )->unsigned();
	        $table->foreign( 'cate_id' )->references( 'id' )->on( 'categories' )->onDelete( 'cascade' );
	        $table->integer( 'user_id' )->unsigned();
	        $table->foreign( 'user_id' )->references( 'id' )->on( 'admin_users' )->onDelete( 'cascade' );
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ideas');
    }
}
