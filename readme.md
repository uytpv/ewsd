#About EWSD Project
EWSD Project is a web application for collecting ideas for improvement from students in a large University.

#Developer Team
1.Tra Phuc Vinh Uy (Scrum master)

2.Vo Hoang Anh

3.Nguyen Ly Minh Man

4.Nguyen Van Son

#WebServer System Requirement

##1. Hardware
###Minimal
* Processor: 2 x 1,6 GHz CPU
* RAM: 2 GB
* HDD: 1 x 20 GB of free space or more

###Recommend
* Processor: 4 x 1,6 GHz CPU
* RAM: 8 GB
* HDD: 1 x 40 GB of free space or more

##2. Software

* Operating system: Window 8.1, Window 10, Ubuntu 16.04
* Database: MySQL 5.0 or higher (recommend for 5.7.19)
* PHP: 7.0 or higher (recommend for 7.1.9 )
* PHP [Composer](https://getcomposer.org/download/)
* Apache recommend for 2.4.27 
* [WAMP](http://www.wampserver.com/en/) 2.2 or [XAMP](https://www.apachefriends.org/download.html) 7.0.28 (recommend for latest version)


#Install instruction

* Using GitHub to clone the ewsd project to your computer.

* Ensure that the ewsd project must be contained in the www folder (used for WAMP Server) or the htdocs folder (used for XAMPP Server).

* In the .evn file, edit database name, username, password.

* The database script was written in **db_ewsd.sql** file.

* Open windows command prompt to move to the ewsd folder, then type **"composer update"** command to update for the ewsd project.

* After the ewsd project was updated. Type **"php artisan serve"** command to run the project.

* **NOTE:** If it does not work correctly, use the **"php artisan key:generate"** to generate new APP_KEY. After that, type **"php artisan serve"** command to run the project again.

* Finally, access the **http://localhost:8000/admin** to login to the ewsd system.
* Username: admin
* Password: admin